<?php
session_start();

$_SESSION["ROOTPATH"] = __DIR__;
include_once ("php/pageManager.php");
include ("php/language.php");
$pageManager = new pageManager("index");
$variables = $pageManager ->loadVariables();
?>
<!DOCTYPE html>
<html lang="<?= $lang ?>">
<head>
  <title><?= $nameFull ?> - <?= $function ?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="keywords" content="<?= $languages['meta.keywords'] ?>">
  <meta name="description" content="<?= $languages['meta.description'] ?> ">
  <meta name="author" content="<?= $nameFull ?>">

  <link rel="alternate" hreflang="en"
        href="https://en.roland-gornisiewicz.pl/" />
  <link rel="alternate" hreflang="de"
        href="https://de.roland-gornisiewicz.pl" />
  <link rel="alternate" hreflang="pl"
        href="https://roland-gornisiewicz.pl" />
  <link rel="alternate" hreflang="x-default"
        href="https://roland-gornisiewicz.pl" />

  <!--<link rel="icon" href="public/img/none.png">-->

  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
  <link rel="stylesheet" href="public/css/main.css?v=1">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  <script src="public/js/googleMaps.js"></script>
  <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116462089-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-116462089-1');
</script>



</head>

<body id='body' data-spy='scroll' data-target='.navbar' data-offset='60'>
<nav class='navbar  navbar-light navbar-expand-sm fixed-top '>
  <!-- Links -->
<!--  <a class='navbar-brand' href='#'>
    <img src='public/IMG/Logonavbar.png' alt='Logo' style='width:100px;'>
  </a>-->
  <a class='navbar-brand' href='#body'>
    RG
  </a>
  <button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#collapsibleNavbar'>
    <span class='navbar-toggler-icon'></span>
  </button>
  <div class='collapse navbar-collapse' id='collapsibleNavbar'>
    <ul class='navbar-nav'>
      <li class='nav-item nav-title'>
        <a class='nav-link' href='#about'><?= $languages['headers.about'] ?></a>
      </li>
      <li class='nav-item nav-title'>
        <a class='nav-link' href='#technology'><?= $languages['headers.technology'] ?></a>
      </li>
      <li class='nav-item nav-title'>
        <a class='nav-link' href='#projects'><?= $languages['headers.projects'] ?></a>
      </li>
      <li class='nav-item nav-title'>
        <a class='nav-link' href='#contact'><?= $languages['headers.contact'] ?></a>
      </li>
    </ul>

    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
        <a href="https://roland-gornisiewicz.pl" ><img class="navbar-flag languageFlag languageFlagPl" src="public/img/pl.png" alt="<?= $languages['headers.polish'] ?>" /></a>
      </li>
      <li class="nav-item">
        <a href="https://en.roland-gornisiewicz.pl"> <img class="navbar-flag languageFlag languageFlagEn" src="public/img/en.png" alt="<?= $languages['headers.english'] ?>" /></a>
      </li>
      <li class="nav-item">
        <a href="https://de.roland-gornisiewicz.pl"> <img class="navbar-flag languageFlag languageFlagDe " src="public/img/de.png" alt="<?= $languages['headers.german'] ?>" /></a>
      </li>
    </ul>



  </div>

</nav>
<main>
  <!--  First blick -->
  <header id='helloWindow' class='jumbotron text-center'>
    <div id='helloWindowTitle' class="helloWindow__container">
        <h1 class='helloWindow__container__h1 slideanim'><b><?=$nameFull?></b></h1>
      <p id='helloWindowSubtitle' class='helloWindow__container__subtitle slideanim'><b><?= $languages['helloWindowSubtitle'] ?></b></p>
    </div>

  </header>

  <!-- Container (About Section) -->
  <section id='about' class='container-fluid'>
    <svg class="dividerBegin" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
      <polygon fill="black" points="0,0 20,101 40,0 60,101 80,0 101,101 120,0 140,101 160,0 180,101 200,0 220,101 240,0 260,101 280,0 300,101 320,0 340,101 360,0 380,101 400,0 420,101 440,0 460,101 480,0  500,101 520,0 540,101 560,0 580,101   600,0 620,101 640,0 660,101 680,0  700,101 720,0 740,101 760,0 780,101 800,0 820,101 840,0 860,101 880,0  900,101 920,0 940,101 960,0 980,101 1000,0"/>
    </svg>
    <header>
      <h2 class='text-center'><?= $languages['headers.about'] ?></h2>
    </header>
    <div class='row'>
      <div class='about__textContainer col-sm-7 pb-4 order-first '>
        <div class="about__textContainer__text">
          <div id="about__textContainer__text_chrome_circle_L" class="d-none  circle circleL float-left"></div><div  id="about__textContainer__text_chrome_circle_R" class="d-none circle circleR float-right"></div>
          <div id="about__textContainer__text_chrome_romb_L" class="d-none rombLeft romb float-left"></div><div id="about__textContainer__text_chrome_romb_R" class="d-none rombRight romb  float-right"></div>
          <p class="about__textContainer__text__p textRomb"><?= $languages['about.aboutMe']?></p>
        </div>

      </div>
      <div class='col-sm-5 about__photoContainer'>
        <svg id="myPhotoSvg" viewBox="0 0 400 400" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
          <g>
            <clipPath id="me">
                <polygon points="0	113	0	273	113	386	273	386	386	273	386	113	273	0	113	0"/>
                Sorry, your browser does not support inline SVG.-->
            </clipPath>
          </g>
          <image clip-path="url(#me)" width="400" height="400" xlink:href="public/img/jaMini.jpg" preserveAspectRatio="xMidYMin slice"></image>
        </svg>
      </div>
      <div class='about__textContainerBtn col-sm-7'>
        <button id="about__textContainerBtn__normal" type="button" class="btn btnOrangeBorderHover btn-sm"><i class="far fa-square" alt='normal'></i></button>
        <button id="about__textContainerBtn__circle" type="button" class="btn btnOrangeBorderHover btn-sm"><i class="far fa-circle" alt='circle'></i></button>
        <button id="about__textContainerBtn__romb" type="button" class="btn btnOrangeBorderHover btn-sm"><i class="fab fa-superpowers" alt='romb'></i></button>
      </div>
    </div>
    <svg class="divider" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
      <polygon fill="black" points="0,101 20,0 40,101 60,0 80,101 101,0 120,101 140,0 160,101 180,0 200,101 220,0 240,101 260,0 280,100 300,0 320,100 340,0 360,100 380,0 400,100 420,0 440,100 460,0 480,100  500,0 520,100 540,0 560,100 580,0   600,100 620,0 640,100 660,0 680,100  700,0 720,100 740,0 760,100 780,0 800,100 820,0 840,100 860,0 880,100  900,0 920,100 940,0 960,100 980,0 1000,100"/>
    </svg>

  </section>

  <!-- Container (technology Section) -->
  <section id='technology' class='container-fluid'>

    <div class='text-center'>
      <header>
        <h2><?= $languages['headers.technology'] ?></h2>
      </header>
      <?php foreach($variables['logos'] as $logosMainArray){?>
        <div class="slideanim">
          <div class='row justify-content-center'>
          <?php foreach($logosMainArray['logos']["logo"] as $logo){?>
            <div class='<?= $logosMainArray['logos']['class'] ?> technology-icons align-self-center my-3'>
               <img class="technology__icon" src="<?= $logo['src'] ?>" alt="<?= $logo['alt'] ?>">
            </div>
          <?php } ?>
          </div>
          <div class="technology__about">
            <h3><?= $languages[$logosMainArray['names']] ?></h3>
            <p class="technology__about__p"><?= $languages[$logosMainArray['about']] ?></p>
          </div>
          <div class="technology__container">
            <hr class="technology__container__hr"/>
          </div>
        </div>
      <?php } ?>
    </div>
    <svg class="divider" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
      <polygon fill="#f2f2f2" points="0,101 20,0 40,101 60,0 80,101 101,0 120,101 140,0 160,101 180,0 200,101 220,0 240,101 260,0 280,100 300,0 320,100 340,0 360,100 380,0 400,100 420,0 440,100 460,0 480,100  500,0 520,100 540,0 560,100 580,0   600,100 620,0 640,100 660,0 680,100  700,0 720,100 740,0 760,100 780,0 800,100 820,0 840,100 860,0 880,100  900,0 920,100 940,0 960,100 980,0 1000,100"/>
    </svg>
  </section>

  <!-- Container (Projects Section) -->
  <section id='projects' class='container-fluid'>
    <header>
      <h2 class='text-center'><?= $languages['headers.projects'] ?></h2>
    </header>
    <div class="container">
      <div class="timeline">
        <?php foreach ($variables["projects"] as $year => $projectArray){?>
          <h2 class="timeline__h2"><?= ((int)$year +1) ?></h2>
          <ul class="timeline__ul">
          <?php foreach ($projectArray as $project){?>
            <li class="timeline__ul__li">
              <h3 class='li__h3'><?= $languages[$project['title']] ?></h3> <!-- daty mam na gicie niektore -->
              <p><?= $languages[$project['descripton']] ?></p>
              <div class="li__technologies">
                <?php foreach ($project['technologies'] as $technology){?>
                  <div class="li__technologies__technology"><?= $technology ?></div>
                <?php } ?>
              </div>


              <time class='li__time'><?= $languages[$project['date']] ?></time>
              <?php if (!empty($project['linkShowLive'])){?>
              <div class='li__iconDiv'>
                <a class='li__iconDiv__a' href="<?=$project['linkShowLive']?>" target="_blank">
                 <i class="icon far fa-eye"></i>
                </a>
              </div>
              <?php }
              if (!empty($project['linkRepository'])){
              ?>
              <div class='li__iconDiv'>
                <a class='li__iconDiv__a' href="<?=$project['linkRepository']?>" target="_blank">
                 <i class="iconDiv__a__icon fab fa-gitlab"></i>
                </a>
              </div>
              <?php } ?>
            </li>
          <?php } ?>
          </ul>
        <?php } ?>
          <h2 class="timeline__h2">2016</h2>
      </div>
    </div>
    <svg class="divider" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
      <polygon fill="black" points="0,101 20,0 40,101 60,0 80,101 101,0 120,101 140,0 160,101 180,0 200,101 220,0 240,101 260,0 280,100 300,0 320,100 340,0 360,100 380,0 400,100 420,0 440,100 460,0 480,100  500,0 520,100 540,0 560,100 580,0   600,100 620,0 640,100 660,0 680,100  700,0 720,100 740,0 760,100 780,0 800,100 820,0 840,100 860,0 880,100  900,0 920,100 940,0 960,100 980,0 1000,100"/>
    </svg>
  </section>

  <!-- Container (Contact Section) -->
  <section id='contact' class='container-fluid position-relative'>

    <header>
      <h2 class='text-center'><?= $languages['headers.contact'] ?></h2>
    </header>
    <div class='row'>
      <div class='col-sm-12 slideanim'>
        <div class='row'>
          <div class='offset-sm-3 col-sm-6 form-group'>
            <input class='form-control' id='mail_email' name='email' placeholder='<?= $languages['yourMail'] ?>' type='email' required>
            <div class="valid-feedback feedback-icon">
              <i class="fa fa-check"></i>
            </div>
            <div class="invalid-feedback feedback-icon">
              <i class="fa fa-times"></i>
            </div>
            <div class="invalid-feedback">
              <?= $languages['validation.invalid.mail'] ?>
            </div>
          </div>
        </div>
        <div class='row'>
          <div class="form-group offset-sm-2 col-sm-8">
          <textarea class='form-control' id='mail_text' name='comments' placeholder='<?= $languages['text'] ?>' rows='5'></textarea>
            <div class="invalid-feedback">
              <?= $languages['validation.cannotempty'] ?>
            </div>
          </div>
          <br>
        </div>
        <div class='row'>
          <div class='mx-auto form-group '>
              <svg id='mail_send' class="liquid-button"
                data-text="<?= $languages['send'] ?>"
                data-text-color="#000000"
                data-force-factor="0.1"
                data-layer-1-viscosity="0.5"
                data-layer-2-viscosity="0.4"
                data-layer-1-mouse-force="400"
                data-layer-2-mouse-force="500"
                data-layer-1-force-limit="1"
                data-layer-2-force-limit="2"
                data-color1="black"
                data-color2="orange"
                data-color3="#ffde21">
              </svg>
          </div>
        </div>
        <!--Dolny panel mediów-->
        <div class="row">
          <div class="d-block d-sm-none slideanim mx-auto position-static contact__media_container container__lower">
            <a href="https://www.linkedin.com/in/roland-górnisiewicz-93214810b" target="_blank"><i class="fab mx-2 fa-linkedin-in"></i></a>
            <a href="https://gitlab.com/Wicher3k" target="_blank"><i class="fab mx-2 rightIcons fa-gitlab"></i></a>
            <a href="https://www.facebook.com/roland.gornisiewicz" target="_blank"><i class="fab mx-2 rightIcons fa-facebook-f"></i></a>
          </div>
        </div>
      </div>
    </div>
    <!-- prawy panel mediow-->
    <div class="d-none d-sm-block position-absolute contact__media_container container__right">
       <a href="https://www.linkedin.com/in/roland-górnisiewicz-93214810b" target="_blank">
         <i class="slideLeftanim rightIcons fab fa-linkedin-in"></i>
       </a>
       <a href="https://gitlab.com/Wicher3k" target="_blank">
         <i class="slideLeftanim rightIcons fab fa-gitlab"></i>
       </a>
       <a href="https://www.facebook.com/roland.gornisiewicz" target="_blank">
         <i class="slideLeftanim rightIcons fab fa-facebook-f"></i>
       </a>
    </div>

  </section>

  <div id='googleMapContact' style='height:350px;width:100%;'></div>
</main><!-- Button trigger modal -->



<footer class='text-center'>
  <a href='#body' title='To Top'>
    <i class='fas fa-chevron-circle-up'></i>
  </a>
  <p><?= $languages['footer.text'] ?> <a href='https://roland-gornisiewicz.pl' title='Portfolio Roland Górnisiewicz' rel="author">Roland Górnisiewicz</a></p>
</footer>
 <!-- Add Google Maps -->


<?php include 'php/modals.php'?>

</body>
<script src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCUjHHN5nRV0IadF1SnMfsXRxqJTh1b7pk&callback=myMap'></script>

<script src='public/js/functions.js'></script>
<script src='public/js/main.js'></script>
<script src='public/js/bubbles.js'></script>
<script src='public/js/liquidButton.js'></script>
</html>