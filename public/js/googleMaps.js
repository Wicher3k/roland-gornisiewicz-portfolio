 function myMap() {
  var myCenterContact = new google.maps.LatLng(50.0646325,19.9449921);
  var mapProp = {center:myCenterContact, zoom:7, scrollwheel:false, draggable:true, mapTypeId:google.maps.MapTypeId.ROADMAP};
  var mapContact = new google.maps.Map(document.getElementById("googleMapContact"),mapProp);

  var myCenterContactMarker = new google.maps.LatLng(50.0646325,19.9449921);
  var marker = new google.maps.Marker({position:myCenterContactMarker});
  marker.setMap(mapContact);
}