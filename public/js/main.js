$(document).ready(function () {

  //variables
  var lang = document.documentElement.lang;

  $('.languageFlag' + lang.charAt(0).toUpperCase() + lang.slice(1)).addClass('active');

  // pokazanie pierwszej strony
  window.setTimeout(function () {
    $('#helloWindowTitle h1').addClass("showEuro");
  }, [500]);
  window.setTimeout(function () {
    $('#helloWindowSubtitle').addClass("showEuro");
  }, [1000]);

  // Add smooth scrolling to all links in navbar + footer link
  $(".navbar a, footer a[href='#body']").on('click', function (event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;
      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (900) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 900, function () {

        // Add hash (#) to URL when done scrolling (default click behavior)
        if (hash === "#body") {
          hash = '';
        }
        window.location.hash = hash;
      });
    } // End if
  });

  // animacja elementów po skrolowaniu
  $(window).scroll(function () {
    $(".slideanim").each(function () {
      let pos = $(this).offset().top;

      let winTop = $(window).scrollTop();
      if (pos < winTop + 600 && !$(this).hasClass("slide")) {
        $(this).addClass("slide");
      }
    });

    $(".slideLeftanim").each(function () {
      let pos = $(this).offset().top;
      let winTop = $(window).scrollTop();
      if (pos < winTop + 600 && !$(this).hasClass("slideLeft") && $(this).css('visibility') === "hidden") {
        $(this).addClass("slideLeft").removeClass('slideLeftReverse');
      }
      if (pos > winTop + 700 && !$(this).hasClass("slideLeftReverse") && $(this).css('opacity') === "1") {
        $(this).addClass("slideLeftReverse").removeClass('slideLeft');
      }
    });
  });

  // blur contact validation

  $('#mail_email').on('blur', function () {
    var $emailObj = $(this);
    $emailObj.removeClass('is-invalid is-valid');
    var email = $emailObj.val();
    if (!validateEmail($emailObj.val()) && email !== '') {
      $emailObj.addClass('is-invalid');
      $emailObj.nextAll('.form-control-feedback').show();
    } else {
      if (email !== '') {
        $emailObj.addClass('is-valid');
      }
    }
  });

  //send mail by ajax
  $('#mail_send').on('click', function () {

    var $emailObj = $('#mail_email'),
            $emailTextObj = $('#mail_text');
    var email = $emailObj.val(),
            text = $emailTextObj.val(),
            sendMail = true;

    if (!validateEmail(email)) {
      $emailObj.addClass('is-invalid');
      sendMail = false;
    } else {
      $emailObj.removeClass('is-invalid');
    }
    if (text === "") {
      $emailTextObj.addClass('is-invalid');
      sendMail = false;
    } else {
      $emailTextObj.removeClass('is-invalid');
    }
    if (sendMail === true) {
      var mailData = {'email': email, 'text': text};
      $.ajax({
        type: 'POST',
        url: 'php/ajax/sendMail.php',
        data: {data: JSON.stringify(mailData)},
        dataType: 'json',
        success: function (data) {
          $('#exampleModalCenter').modal();
        }
      });
    }
  });

  // hover border direction
  var indexNavLink = -1;
  $('.nav-item').hover(function () {
    let newIndexNavLink = $(this).index();
    if (newIndexNavLink > indexNavLink) {
      $(this).removeClass('left').addClass('right');
    } else if (newIndexNavLink < indexNavLink) {

      $(this).removeClass('right').addClass('left');

    }
    indexNavLink = newIndexNavLink;
  });

  //zmiana polezenia tekstu - obsluga guzikow
  $('#about__textContainerBtn__normal').on('click', function () {
    about__textContainerBtn_normal($(this));
  });
  $('#about__textContainerBtn__circle').on('click', function () {
    about__textContainerBtn_circle($(this));
  });
  $('#about__textContainerBtn__romb').on('click', function () {
    about__textContainerBtn_romb($(this));
  });
  let versionStyleAboutText = getRandomInt(0, 2);
  switch (versionStyleAboutText) {
    case 2:
      $('#about__textContainerBtn__romb').trigger('click');
      break;
    case 1:
      $('#about__textContainerBtn__circle').trigger('click');
      break;
    case 0:
    default:
      $('#about__textContainerBtn__normal').trigger('click');
      break;
  }


});

function validateEmail(email) {
  var email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
  if (email_regex.test(email)) {
    return true;
  } else {
    return false;
  }
}

function about__textContainerBtn_normal($objNormal) {
  $('.about__textContainerBtn .btn').removeClass('btnSelected');
  $('#about__textContainer__text_chrome_circle_L,#about__textContainer__text_chrome_circle_R').addClass('d-none');
  $('#about__textContainer__text_chrome_romb_L,#about__textContainer__text_chrome_romb_R').addClass('d-none');
  $objNormal.addClass('btnSelected');
}
function about__textContainerBtn_circle($objCircle) {
  $('.about__textContainerBtn .btn').removeClass('btnSelected');
  $('#about__textContainer__text_chrome_circle_L,#about__textContainer__text_chrome_circle_R').removeClass('d-none');
  $('#about__textContainer__text_chrome_romb_L,#about__textContainer__text_chrome_romb_R').addClass('d-none');
  $objCircle.addClass('btnSelected');
}

function about__textContainerBtn_romb($objromb) {
  $('.about__textContainerBtn .btn').removeClass('btnSelected');
  $('#about__textContainer__text_chrome_romb_L,#about__textContainer__text_chrome_romb_R').removeClass('d-none');
  $('#about__textContainer__text_chrome_circle_L,#about__textContainer__text_chrome_circle_R').addClass('d-none');
  $objromb.addClass('btnSelected');
}