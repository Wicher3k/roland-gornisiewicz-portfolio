 // bąbelki
var colorArray = ['radial_black2orange','radial_rainbow'];
$(function() {
  var isIE = /*@cc_on!@*/false || !!document.documentMode;
  var isEdge = !isIE && !!window.StyleMedia;
  
  generateRandomBubbles(500); // a wygeneruj sobie losowe
  
  //mousemove - wylicza wspolrzedne babelka
  $("#helloWindow").on("mousemove",function (e) {
    if(isEdge || isIE){ // na edge zacinało sie przy wiekszym obciazeniu
      if (!Math.floor(e.timeStamp%2)==0){
         return;
      }
    }
    var $helloWindow = $(this);
    // Setup
    var posX = $helloWindow.offset().left,
        posY = $helloWindow.offset().top,
        buttonWidth = $helloWindow.width()/10,
        buttonHeight =  $helloWindow.height()/10,
        diameter=0,
        x=0,
        y=0,
        color = randomColor(colorArray);

   // Make it round!
    if(buttonWidth >= buttonHeight) {
      diameter = buttonWidth;
    } else {
      diameter = buttonHeight; 
    }

    // Get the center of the element
    x = e.pageX - posX - diameter / 2;
    y = e.pageY - posY - diameter / 2;
    drawBubble($helloWindow,x,y,diameter,color);
   
  });
});
  
//do generowania wlasnych babelkow - wyliczanie pozycji babelka
function generateRandomBubbles(bubblesNumber){

  if(bubblesNumber===0){ //jezeli zero to koniec generowania
    return;
  }
  
  window.setTimeout(function(){
    var $helloWindow = $('#helloWindow'); // musi byc zawsze osobno lcizone jakby ktos resiza robił
    var helloWindowHeight = $helloWindow.height(),
    helloWindowWidth = $helloWindow.width();
    
    var babelekLeft = Math.floor(Math.random() * (helloWindowWidth*0.924)), //1/10 zajmuje wielkość wiec zeby nie wyskakiwao poza ekran/ ale sa skalowane do dwóch więc 0.9->0.95->0.925 *dla 0.925 lekko wyskakiwalo poza border wiec 942
    babelekTop = Math.floor(Math.random() * (helloWindowHeight*0.924)), 
    color = randomColor(colorArray),
    diameter=0;
    
    // Make it round!
    if(helloWindowHeight >= helloWindowWidth) {
      diameter = helloWindowHeight/10;
    } else {
      diameter = helloWindowWidth/10; 
    }
    
    drawBubble($helloWindow,babelekLeft,babelekTop,diameter,color);

    generateRandomBubbles(bubblesNumber-1);
  }, [2]);
}

//funkcja do wyswietlania babelka i dodaje do niego rippleEffect
indexBubble=0;
function drawBubble($helloWindow,bubblePossitionLeft,bubblePossitionTop,diameter,color){
    // Add the element
    $helloWindow.prepend("<span id='ripple"+indexBubble+"' class='ripple "+color+"'></span>");
    // Add the ripples CSS and start the animation
    let $ripple = $("#ripple"+indexBubble);
    $ripple.css({
      width: diameter,
      height: diameter,
      top: bubblePossitionTop + 'px',
      left: bubblePossitionLeft + 'px'
    }).addClass("rippleEffect");
    
    if(color==='radial_rainbow'){
      
      $ripple.css("background","radial-gradient( rgba(0,0,0,1) 50%, rgba(0,0,0,0) 90%) no-repeat border-box, linear-gradient("+getRandomInt(0,360)+"deg,red,red, orange , yellow, green, cyan, blue, violet, violet)");
    }

    window.setTimeout(function($ripple2Del){
      $ripple2Del.remove();
    },3000,$ripple);
    
    indexBubble+=1;
}

function randomColor(colorArray){
  //wylosuj 1:100 rainbow
    if(Math.floor(Math.random() * 100)==1){
      return colorArray[1];
    }else{
      return colorArray[0];
    }
}
