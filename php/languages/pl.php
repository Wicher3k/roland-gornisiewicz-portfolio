<?php



$languages=[
    //meta
    'meta.description' => "$nameFull - Portfolio - $function. Programista stron i aplikacji internetowych", // to co pokazuje sie w google Krakow",
    'meta.keywords' => "Roland, Górnisiewicz, Wicher3k, Wicher, Roland, Górnisiewicz AGH, Rolci, Akademia Górniczo Hutnicza, Web Developer, Full Stack, Full-Stack, Web, CSS, CSS3, JavaScript, HTML, HTML5, Laravel, MongoDB, PHP, MySQL, Strony internetowe, aplikacje internetowe, jQuery, webdesign, portfolio, informatyk, Angular 4, WordPress, Kraków",
//    //ogolne
    'name' => 'Imię',
    'text' => 'Tekst',
    'yourMail' => 'Twój email',
    'send' => "Wyślij",
    'success' => "Sukces",
    'OK' => "OK",

    //wiadomosci
    'message.message' => "Wiadomość wysłana",

    //walidacja
    'validation.invalid.mail' => "Niepoprawny email!",
    'validation.cannotempty' => "Pole nie może być puste!",

    //headers
    'headers.about' => 'O mnie',
    'headers.technology' => 'Technologie',
    'headers.projects' => 'Projekty',
    'headers.contact' => 'Kontakt',

    'headers.polish' => 'Polski',
    'headers.english' => 'Angielski',
    'headers.german' => 'Niemiecki',

    'helloWindowSubtitle' => 'Web Developer',

    'about.aboutMe' => "Cześć, nazywam się $nameFull. Ukończyłem studia na Akademii Górniczo-Hutniczej II&nbsp;stopnia w Krakowie. Jestem $function - osobą zajmującą się programowaniem po stronie przeglądarki, serwera oraz bazy danych - dosłownie wszystkim czego potrzebujesz do idealnie działającej strony czy aplikacji internetowej.", //W&nbsp;przerwach amatorsko gram w&nbsp;squosha, pływam, biegam i tańczę.

    //technology
    'technology.technologies1' => "HTML5 | CSS3 | JavaScript",
    'technology.technologies2' => "Bootstrap | SASS | jQuery | PHP | MySQL | Python | AJAX | Angular 4",
    'technology.technologies3' => "Gimp | Trello | osTicket | Git",
    'technology.technologiesAbout1' => "Bardzo dobra znajomość języków i technologii umożliwiające tworzenie interaktywnych i świetnie wyglądających stron i aplikacji w standardzie HTML5.",
//    'technology.technologiesAbout2' => "Stosowanie technologii technologii ułatwiających i usprawniających tworzenie witryn.",
    'technology.technologiesAbout2' => "Specjalizuję się w powyższych technologiach ułatwiających i usprawniających tworzenie witryn.",
    'technology.technologiesAbout3' => "Praca ze zdalnym repozytorium. Korzystanie z aplikacji i narzędzi uefektywniająch zarządzanie zadaniami i projektami. Obsługa programu Gimp w celu modyfykacji obrazów.",


    //projects
    'projects.2016.1.title' => 'Pierwsze Portfolio',
    'projects.2016.1.descripton' => 'Moje pierwsze portfolio wystawione na hostingu.',
    'projects.2016.1.date' => 'Marzec 2016',

    'projects.2017.1.title' => 'Praca inżynierska',
    'projects.2017.1.descripton' => 'pt. <i>Zastosowanie algorytmów ewolucyjnych w szczególnym zagadnieniu harmonogramowania</i>',//Evolutionary algorithms for specific scheduling problem
    'projects.2017.1.date' => 'Styczeń 2017',

    'projects.2017.2.title' => 'Publikacja',
    'projects.2017.2.descripton' => '<i>Propozycja zastosowania sieci neuronowych typu FeedForward wraz z metodą klasyfikacji tekstu BOW w audycie dokumentów kadrowych - analiza przypadku </i>. Współautor publikacji oraz stworzonego programu.', //(Proposal for the Experimental Use of FeedForward Neural Networks Together with a BOW Text Classification Method in the Audit of Staff Documentation – A Case Study)
    'projects.2017.2.date' => 'Lato 2017',

    'projects.2018.1.title' => 'Sklep Illenium',
    'projects.2018.1.descripton' => 'Projekt niekomercyjny - Pełnofunkcjonalny sklep internetowy',
    'projects.2018.1.date' => 'Styczeń 2018',

    'projects.2018.2.title' => 'Praca Magisterska',
    'projects.2018.2.descripton' => '<i>Zastosowanie uczenia maszynowego w&nbsp;zagadnieniu kategoryzacji dokumentów</i> za pomocą języka Python 2.7, artykułów polskiej <a href="https://pl.wikipedia.org" target="_blank">Wikipedii</a> oraz wordnetu <a href="http://plwordnet.pwr.wroc.pl/wordnet/" target="_blank">Słowosieci</a>. ',
    'projects.2018.2.date' => 'Lipiec 2018',

    'projects.2018.3.title' => 'ECSI SPEDYCJA Sp. z o.o.',
    'projects.2018.3.descripton' => 'Projekt Komercyjny - Stworzenie storny internetowej dla firmy ECSI SPEDYCJA Sp.&nbsp;z&nbsp;o.o.',
    'projects.2018.3.date' => 'Listopad 2018',

    'projects.2019.1.title' => 'Aktualne Portfolio',
    'projects.2019.1.descripton' => 'Strona, którą teraz przeglądasz :).',
    'projects.2019.1.date' => 'Luty 2019',

    'projects.2020.1.title' => 'My Flashcard Place',
    'projects.2020.1.descripton' => 'Strona do tworzenia i nauki fiszek.',
    'projects.2020.1.date' => 'Maj 2020',


    //footer
    'footer.text' => "Strona stworzona przez"


];

