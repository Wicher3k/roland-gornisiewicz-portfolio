<?php
$languages=[

//     //meta
    'meta.description' => "$nameFull - Portfolio - $function. Websites and web applications programmer.", // to co pokazuje sie w google
    'meta.keywords' => "Roland, Górnisiewicz, Wicher3k, Wicher, Roland, Górnisiewicz AGH, Rolci, Akademia Górniczo Hutnicza, Web Developer, Full Stack, Full-Stack, Web, CSS, CSS3, JavaScript, HTML, HTML5, Laravel, MongoDB, PHP, MySQL, webpages, web applications, jQuery, webdesign, portfolio, programmer, Angular 4, WordPress, Cracow",
//
//    //ogolne
    'name' => 'Name',
    'text' => 'Text',
    'yourMail' => 'Your email',
    'send' => "Send",
    'success' => "Success",
    'OK' => "OK",

    //wiadomosci
    'message.message' => "Message sent",

    //walidacja
    'validation.invalid.mail' => "Incorrect email!",
    'validation.cannotempty' => "The field can not be empty!",

    //headers
    'headers.about' => 'About',
    'headers.technology' => 'Technologies',
    'headers.projects' => 'Projects',
    'headers.contact' => 'Contact',

    'headers.polish' => 'Polish',
    'headers.english' => 'English',
    'headers.german' => 'German',

    'helloWindowSubtitle' => 'Web Developer',

    'about.aboutMe' => "Hi, my name is $nameFull. I graduated from the AGH University of Science and Technology in Kraków. I am a $function - a person involved in programming on the side of the browser, server and database - literally, all what you need for a perfectly working website or application.",

//technology
    'technology.technologies1' => "HTML5 | CSS3 | JavaScript",
    'technology.technologies2' => "Bootstrap | SASS | jQuery | PHP | MySQL | Python | AJAX | Angular 4",
    'technology.technologies3' => "Gimp | Trello | osTicket | Git",
    'technology.technologiesAbout1' => "Very good knowledge of languages and technologies to create interactive and great-looking websites and applications in the HTML5 standard.",
//    'technology.technologiesAbout2' => "Stosowanie technologii technologii ułatwiających i usprawniających tworzenie witryn.",
    'technology.technologiesAbout2' => "I specialize in the above technologies that facilitate and improve the creation of websites.",
    'technology.technologiesAbout3' => "Work with a remote repository. Using applications and tools for effective management of tasks and projects. Gimp support for image modification.",

//projects
    'projects.2016.1.title' => 'The first Portfolio',
    'projects.2016.1.descripton' => 'My first portfolio displayed on hosting.',
    'projects.2016.1.date' => 'March 2016',

    'projects.2017.1.title' => 'Engineer\'s Thesis',
    'projects.2017.1.descripton' => '<i>Evolutionary algorithms for specific scheduling problem</i>',//Evolutionary algorithms for specific scheduling problem
    'projects.2017.1.date' => 'January 2017',

    'projects.2017.2.title' => 'Publication',
    'projects.2017.2.descripton' => '<i>Proposal for the Experimental Use of FeedForward Neural Networks Together with a BOW Text Classification Method in the Audit of Staff Documentation – A Case Study</i>. Co-author of the publication and the created program.', //(Proposal for the Experimental Use of FeedForward Neural Networks Together with a BOW Text Classification Method in the Audit of Staff Documentation – A Case Study)
    'projects.2017.2.date' => 'Summer 2017',

    'projects.2018.1.title' => 'Shop Illenium',
    'projects.2018.1.descripton' => 'Non-commercial project - A full-featured online store',
    'projects.2018.1.date' => 'January 2018',

    'projects.2018.2.title' => 'Master’s thesis',
    'projects.2018.2.descripton' => '<i>Machine learning in the documents categorization</i> using Python 2.7, the <a href="https://pl.wikipedia.org" target="_blank">Polish Wikipedia</a> articles and wordnet <a href="http://plwordnet.pwr.wroc.pl/wordnet/" target="_blank">Słowosieć</a>. ',
    'projects.2018.2.date' => 'July 2018',

    'projects.2018.3.title' => 'ECSI SPEDYCJA Sp. z o.o.',
    'projects.2018.3.descripton' => 'Commercial project - Creating a website for ECSI SPEDYCJA Sp. z o.o.',
    'projects.2018.3.date' => 'November 2018',

    'projects.2019.1.title' => 'Current Portfolio',
    'projects.2019.1.descripton' => 'The page you are currently viewing :).',
    'projects.2019.1.date' => 'March 2019',

    'projects.2020.1.title' => 'My Flashcard Place',
    'projects.2020.1.descripton' => 'Website for creating and learning flashcards.',
    'projects.2020.1.date' => 'May 2020',


    //footer
    'footer.text' => "Website created by"

];

