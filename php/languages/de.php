<?php



$languages=[
    //meta
    'meta.description' => "$nameFull - Portfolio - $function. Websites und Webanwendungen Programmer", // to co pokazuje sie w google
    'meta.keywords' => "Roland, Górnisiewicz, Wicher3k, Wicher, Roland, Górnisiewicz AGH, Rolci, Akademia Górniczo Hutnicza, Web Developer, Full Stack, Full-Stack, Web, CSS, CSS3, JavaScript, HTML, HTML5, Laravel, MongoDB, PHP, MySQL, Websites, Internetanwendungen, jQuery, webdesign, portfolio, Programmierer, Angular 4, WordPress, Krakau",
//
//    //ogolne
    'name' => 'Name',
    'text' => 'Text',
    'yourMail' => 'Ihre E-Mail',
    'send' => "Senden",
    'success' => "Erfolg",
    'OK' => "OK",

    //wiadomosci
    'message.message' => "Nachricht gesendet",

    //walidacja
    'validation.invalid.mail' => "Falsche E-Mail",
    'validation.cannotempty' => "Das Feld darf nicht leer sein!",

    'headers.about' => 'Ich',
    'headers.technology' => 'Technologien',
    'headers.projects' => 'Projekte',
    'headers.contact' => 'Kontakt',

    'headers.polish' => 'Polnisch',
    'headers.english' => 'Englisch',
    'headers.german' => 'Deutsch',

    'helloWindowSubtitle' => 'Web Developer',

    'about.aboutMe' => "Hallo, ich heiße $nameFull. Ich bin der Absolvent der AGH Universität für Wissenschaft und Technologie in Krakau. Ich bin $function - einer Person, die an der Programmierung von Browser, Server und Datenbank mitwirkt - alles, was Sie für eine perfekt funktionierende Website oder Anwendung benötigen.",

    //technology
    'technology.technologies1' => "HTML5 | CSS3 | JavaScript",
    'technology.technologies2' => "Bootstrap | SASS | jQuery | PHP | MySQL | Python | AJAX | Angular 4",
    'technology.technologies3' => "Gimp | Trello | osTicket | Git",
    'technology.technologiesAbout1' => "Sehr gute Kenntnisse der Sprachen und Technologien zur Erstellung interaktiver und gut aussehender Websites und Anwendungen im HTML5-Standard.",
//    'technology.technologiesAbout2' => "Stosowanie technologii technologii ułatwiających i usprawniających tworzenie witryn.",
    'technology.technologiesAbout2' => "Ich bin auf die oben genannten Technologien spezialisiert, die die Erstellung von Websites erleichtern und verbessern.",
    'technology.technologiesAbout3' => "Arbeiten mit einem Remote-Repository. Verwendung von Anwendungen und Tools zur effektiven Verwaltung von Aufgaben und Projekten. Gimp-Unterstützung für die Bildänderung.",

//projects
    'projects.2016.1.title' => 'Das erste Portfolio',
    'projects.2016.1.descripton' => 'Mein erstes Portfolio wird beim Hosting angezeigt.',
    'projects.2016.1.date' => 'März 2016',

    'projects.2017.1.title' => 'Bachelorarbeit',
    'projects.2017.1.descripton' => '<i>Die Verwendung evolutionärer Algorithmen in einem speziellen Planungsproblem</i>',//Evolutionary algorithms for specific scheduling problem
    'projects.2017.1.date' => 'Januar 2017',

    'projects.2017.2.title' => 'Publikation',
    'projects.2017.2.descripton' => '<i>Proposal for the Experimental Use of FeedForward Neural Networks Together with a BOW Text Classification Method in the Audit of Staff Documentation – A Case Study </i>. Mitautor der Publikation und des erstellten Programms.', //(Proposal for the Experimental Use of FeedForward Neural Networks Together with a BOW Text Classification Method in the Audit of Staff Documentation – A Case Study)
    'projects.2017.2.date' => 'Sommer 2017',

    'projects.2018.1.title' => 'Geschäft Illenium',
    'projects.2018.1.descripton' => 'Nichtkommerzielles Projekt - Ein voll ausgestatteter Online-Shop',
    'projects.2018.1.date' => 'Januar 2018',

    'projects.2018.2.title' => 'Magisterarbeit',
    'projects.2018.2.descripton' => '<i>Maschinelles Lernen in der Kategorisierung von Dokumenten</i> mithilfe von Python 2.7, der polnischen <a href="https://pl.wikipedia.org" target="_blank">Wikipedia-Artikel</a> und Wordnet <a href="http://plwordnet.pwr.wroc.pl/wordnet/" target="_blank">Słowosieć</a>. ',
    'projects.2018.2.date' => 'Juli 2018',

    'projects.2018.3.title' => 'ECSI SPEDYCJA Sp. z o.o.',
    'projects.2018.3.descripton' => 'Kommerzielles Projekt - Erstellen einer Website für ECSI SPEDYCJA Sp. z o.o.',
    'projects.2018.3.date' => 'November 2018',

    'projects.2019.1.title' => 'Aktuelles Portfolio',
    'projects.2019.1.descripton' => 'Die Seite, die Sie gerade anzeigen :).',
    'projects.2019.1.date' => 'März 2019',

    'projects.2020.1.title' => 'My Flashcard Place',
    'projects.2020.1.descripton' => 'Website zum Erstellen und Lernen von Lernkarten.',
    'projects.2020.1.date' => 'Mai 2020',


    //footer
    'footer.text' => "Website erstellt von"

];

