<?php

class pageManager
{

  private $page;
  public $variables = [];

  public function __construct($page)
  {
    $this->page = $page;
  }

  public function loadVariables()
  {

    if ($this->page == "index") {

      //logos
      $logoSrc = "public/img/logos/logo";
      $logoSrcEnd = ".png";
      $logoAlt = "logo";
      $logos = [
        "1" => [
          "logos" => [
            "logo" => [
              ["src" => $logoSrc . "HTML" . $logoSrcEnd, "alt" => 'HTML' . $logoAlt],
              ["src" => $logoSrc . "CSS" . $logoSrcEnd, "alt" => 'CSS' . $logoAlt],
              ["src" => $logoSrc . "JS" . $logoSrcEnd, "alt" => 'JS' . $logoAlt]
            ],
            "class" => 'col-sm-4 col-6'
          ],
          "names" => 'technology.technologies1',
          "about" => 'technology.technologiesAbout1'
        ],
        "2" => [
          "logos" => [
            "logo" => [
              ["src" => $logoSrc . "Bootstrap" . $logoSrcEnd, "alt" => 'Bootstrap' . $logoAlt],
              ["src" => $logoSrc . "SASS" . $logoSrcEnd, "alt" => 'Sass' . $logoAlt],
              ["src" => $logoSrc . "JQuery" . $logoSrcEnd, "alt" => 'JQuery' . $logoAlt],
              ["src" => $logoSrc . "PHP" . $logoSrcEnd, "alt" => 'PHP' . $logoAlt],
              ["src" => $logoSrc . "MySQL" . $logoSrcEnd, "alt" => 'MySQL' . $logoAlt],
              ["src" => $logoSrc . "Python" . $logoSrcEnd, "alt" => 'Python' . $logoAlt],
              ["src" => $logoSrc . "Ajax" . $logoSrcEnd, "alt" => 'AJAX' . $logoAlt],
              ["src" => $logoSrc . "Angular" . $logoSrcEnd, "alt" => 'Angular' . $logoAlt],
            ],
            "class" => 'col-md-3 col-sm-4 col-6'
          ],
          "names" => 'technology.technologies2',
          "about" => 'technology.technologiesAbout2'
        ],
        "3" => [
          "logos" => [
            "logo" => [
              ["src" => $logoSrc . "Gimp" . $logoSrcEnd, "alt" => 'Gimp' . $logoAlt],
              ["src" => $logoSrc . "Trello" . $logoSrcEnd, "alt" => 'Trello' . $logoAlt],
              ["src" => $logoSrc . "OsTicket" . $logoSrcEnd, "alt" => 'OsTicket' . $logoAlt],
              ["src" => $logoSrc . "Git" . $logoSrcEnd, "alt" => 'Git' . $logoAlt]
            ],
            "class" => 'col-md-3 col-sm-4 col-6'
          ],
          "names" => 'technology.technologies3',
          "about" => 'technology.technologiesAbout3'
        ]
      ];
      $variables["logos"] = $logos;

      //projects

      $projects = [
        "2020" =>  [
          [
            'title' => 'projects.2020.1.title',
            'descripton' => 'projects.2020.1.descripton',
            'technologies' => ['HTML5', 'CSS3', 'SASS', 'Bootstrap4', 'JavaScript', 'jQuery', 'PHP', "Laravel"],
            'linkShowLive' => 'https://myflashcardplace.com',
            'date' => 'projects.2020.1.date'
          ],
        ],
        "2019" =>  [
          [
            'title' => 'projects.2019.1.title',
            'descripton' => 'projects.2019.1.descripton',
            'technologies' => ['HTML5', 'CSS3', 'SASS', 'Bootstrap4', 'JavaScript', 'jQuery', 'PHP'],
            'linkRepository' => 'https://gitlab.com/Wicher3k/roland-gornisiewicz-portfolio',
            'linkShowLive' => 'https://roland-gornisiewicz.pl',
            'date' => 'projects.2019.1.date'
          ],
        ],
        "2018" => [
          [
            'title' => 'projects.2018.3.title',
            'descripton' => 'projects.2018.3.descripton',
            'technologies' => ['HTML5', 'CSS3', 'SASS', 'Bootstrap4', 'jQuery', 'PHP'],
            'linkRepository' => '',
            'linkShowLive' => 'https://ecsispedycja.pl',
            'date' => 'projects.2018.3.date'
          ],
          [
            'title' => 'projects.2018.2.title',
            'descripton' => 'projects.2018.2.descripton',
            'technologies' => ['Python (Sklearn, Keras, Numpy, OCR, Gensim&nbsp;[W2V,D2V], igraph)', 'XML', 'Xpath'],
            'linkRepository' => 'https://gitlab.com/Wicher3k/pracaMagisterska',
            'linkShowLive' => '',
            'date' => 'projects.2018.2.date'
          ],
          [
            'title' => 'projects.2018.1.title',
            'descripton' => 'projects.2018.1.descripton',
            'technologies' => ['MongoDB', 'Angular4', 'Node.js', 'Bootstrap3', 'HTML5', 'CSS3'],
            'linkRepository' => 'https://gitlab.com/Wicher3k/Internet_Shop',
            'linkShowLive' => 'https://peaceful-ocean-60282.herokuapp.com/',
            'date' => 'projects.2018.1.date'
          ],
        ],
        "2017" => [
          [
            'title' => 'projects.2017.2.title',
            'descripton' => 'projects.2017.2.descripton',
            'technologies' => ['Python (Keras, OCR, BoW)', 'MySQL'],
            'linkRepository' => '',
            'linkShowLive' => 'https://cfm.uek.krakow.pl/media/files/fe/80/FINANSE_%20CFM%202017.pdf#page=81',
            'date' => 'projects.2017.2.date'
          ],
          [
            'title' => 'projects.2017.1.title',
            'descripton' => 'projects.2017.1.descripton',
            'technologies' => ['Python (Numpy, Matplotlib, Tkinter, pyInstaller)'],
            'linkRepository' => 'https://gitlab.com/Wicher3k/praca-inzynierska',
            'linkShowLive' => '',
            'date' => 'projects.2017.1.date'
          ],
        ],
        "2016" => [
          [
            'title' => 'projects.2016.1.title',
            'descripton' => 'projects.2016.1.descripton',
            'technologies' => ['HTML', 'CSS', 'PHP', 'JavaScript', 'jQuery'],
            'linkRepository' => 'https://gitlab.com/Wicher3k/old-portfolio',
            'linkShowLive' => 'https://roland-gornisiewicz.pl/oldPortfolio/',
            'date' => 'projects.2016.1.date'
          ],
        ],
      ];
      $variables["projects"] = $projects;
      return $variables;
    }
  }
}
