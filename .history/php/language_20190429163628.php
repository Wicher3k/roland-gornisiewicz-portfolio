<?php

$lang = NULL;

if( preg_match( '/^(.+)\.roland-gornisiewicz.pl$/' , $_SERVER['HTTP_HOST'] , $matches )
    && count( $matches )==2
    && $matches[1]!='www' ){
  $lang = $matches[1];
}

$acceptLang = ['pl', 'de', 'en']

$lang = in_array($lang, $acceptLang) ? $lang : 'pl';

include_once 'php/languages/constantLanguages.php';
include_once 'php/languages/'.$lang.'.php';